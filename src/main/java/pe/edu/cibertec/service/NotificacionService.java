package pe.edu.cibertec.service;

import java.util.List;

import pe.edu.cibertec.dto.NotificacionxAsignarUsuariosDTO;
import pe.edu.cibertec.dto.UsuarioNotificacionDTO;
import pe.edu.cibertec.model.Categoria;
import pe.edu.cibertec.model.Notificacion;

public interface NotificacionService {

	public List<Notificacion> obtenerNotificacionesxCategoria(Long idCategoria);
	
	public UsuarioNotificacionDTO obtenerNotificacionesxUsuarioxCategoria(Long idUsuario, Long idCategoria);
	
	public void eliminarNotificacionUsuario(List<Long> listaUsuNotIds); 
	
	public void enviarNotificacionUsuarios(NotificacionxAsignarUsuariosDTO nnudto);
	
	public void marcarNotificacionComoLeida(Long idUsuarioNotificacion);
	
	public List<Categoria> obtenerCategorias();
}
