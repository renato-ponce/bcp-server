package pe.edu.cibertec.service;

import pe.edu.cibertec.model.Usuario;

public interface UsuarioService {

	public Usuario insertarUsuario(Usuario usuario);
	
	public Usuario actualizarUsuario(Usuario usuario);
	
	public Usuario validarLogin(Long tarjeta, int password);
}
