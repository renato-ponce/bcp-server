package pe.edu.cibertec.service.impl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.cibertec.service.UsuarioService;
import pe.edu.cibertec.model.Notificacion;
import pe.edu.cibertec.model.Usuario;
import pe.edu.cibertec.model.UsuarioNotificacion;
import pe.edu.cibertec.repository.NotificacionRepository;
import pe.edu.cibertec.repository.UsuarioCustomRepository;
import pe.edu.cibertec.repository.UsuarioNotificacionRepository;
import pe.edu.cibertec.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UsuarioCustomRepository usuarioCustomRepository;

	@Autowired
	private NotificacionRepository notificacionRepository;

	@Autowired
	private UsuarioNotificacionRepository usuarioNotificacionRepository;

	@Override
	public Usuario insertarUsuario(Usuario usuario) {
		// Usuario Registrado en BCP
		Usuario usuarioInsertado = usuarioRepository.save(usuario);

		// Le asignamos la notificacion de Bienvenida
		UsuarioNotificacion un = new UsuarioNotificacion();
		Notificacion not = notificacionRepository.findById(1L).get();
		un.setNotificacion(not);
		un.setUsuario(usuarioInsertado);
		un.setFecha(LocalDate.now());

		usuarioNotificacionRepository.save(un);
		
		usuarioInsertado.setPassword(0); //No devolver Password
		return usuarioInsertado;
	}

	@Override
	public Usuario actualizarUsuario(Usuario usuario) {
		// Recibe Objeto Usuario
		Long IdUsuario = usuario.getIdUsuario();

		Usuario usubd = usuarioRepository.findById(IdUsuario).get();
		// Seteamos los valores de ese Objeto
		usubd.setNombre(usuario.getNombre());
		usubd.setApellido(usuario.getApellido());
		usubd.setEmail(usuario.getEmail());
		usubd.setPassword(usuario.getPassword());

		Usuario usuarioActualizado = usuarioRepository.save(usubd);

		usuarioActualizado.setPassword(0); //No devolver password
		return usuarioActualizado;
	}

	@Override
	public Usuario validarLogin(Long tarjeta, int password) {
		Usuario usuario = usuarioCustomRepository.findByTarjetaAndPassword(tarjeta, password);
		
		if(usuario!=null) usuario.setPassword(0); //No devolver password
		return usuario;
	}

}
