package pe.edu.cibertec.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.cibertec.service.NotificacionService;
import pe.edu.cibertec.dto.NotificacionxAsignarUsuariosDTO;
import pe.edu.cibertec.dto.UsuarioNotificacionDTO;
import pe.edu.cibertec.model.Categoria;
import pe.edu.cibertec.model.Notificacion;
import pe.edu.cibertec.model.Usuario;
import pe.edu.cibertec.model.UsuarioNotificacion;
import pe.edu.cibertec.repository.CategoriaRepository;
import pe.edu.cibertec.repository.NotificacionCustomRepository;
import pe.edu.cibertec.repository.NotificacionRepository;
import pe.edu.cibertec.repository.UsuarioNotificacionCustomRepository;
import pe.edu.cibertec.repository.UsuarioNotificacionRepository;
import pe.edu.cibertec.repository.UsuarioRepository;

@Service
public class NotificacionServiceImpl implements NotificacionService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private NotificacionRepository notificacionRepository;
	@Autowired
	private CategoriaRepository categoriaRepository;

	@Autowired
	private UsuarioNotificacionCustomRepository usuarioNotificacionCustomRepository;

	@Autowired
	private UsuarioNotificacionRepository usuarioNotificacionRepository;
	@Autowired
	private NotificacionCustomRepository notificacionCustomRepository;

	@Override
	public List<Notificacion> obtenerNotificacionesxCategoria(Long idCategoria) {
		Optional<Categoria> cat = categoriaRepository.findById(idCategoria);

		List<Notificacion> lista = new ArrayList<Notificacion>();

		if (cat.isEmpty()) {
			lista = (List<Notificacion>) notificacionRepository.findAll();
		} else {
			Categoria categoria = cat.get();
			lista = notificacionCustomRepository.findByCategoria(categoria);
		}

		return lista;
	}

	@Override
	public UsuarioNotificacionDTO obtenerNotificacionesxUsuarioxCategoria(Long idUsuario, Long idCategoria) {
		Usuario usuario = usuarioRepository.findById(idUsuario).get();

		List<UsuarioNotificacion> lista = new ArrayList<UsuarioNotificacion>();

		Optional<Categoria> cat = categoriaRepository.findById(idCategoria);

		if (cat.isEmpty()) {
			lista = usuarioNotificacionCustomRepository.findByUsuarioOrderByFechaDesc(usuario);
		} else {
			Categoria categoria = cat.get();
			lista = usuarioNotificacionCustomRepository.findByUsuarioAndNotificacion_CategoriaOrderByFechaDesc(usuario,
					categoria);

		}
		Long cantidad = 0L;
		for (UsuarioNotificacion usuarioNotificacion : lista) {
			if (usuarioNotificacion.isEstado() == false) {
				cantidad += 1;
			}
		}
		UsuarioNotificacionDTO undto = new UsuarioNotificacionDTO();
		
		undto.setCantidadNoLeidas(cantidad);
		undto.setListaNotificaciones(lista);
		
		return undto;
	}

	@Override
	public void eliminarNotificacionUsuario(List<Long> listaUsuNotIds) {
		List<UsuarioNotificacion> listado = usuarioNotificacionRepository.findAllById(listaUsuNotIds);

		usuarioNotificacionRepository.deleteAll(listado);
	}

	@Override
	public void enviarNotificacionUsuarios(NotificacionxAsignarUsuariosDTO nnudto) {
		// Recibe en un DTO: Listado, IdCat, Desc, IdNot, Desc
		List<Long> listaIdUsuarios = nnudto.getListaUsuarios();

		Long idCategoria = nnudto.getIdCategoria();

		Long idNotificacion = nnudto.getIdNotificacion();

		List<Usuario> listaUsuarios = (List<Usuario>) usuarioRepository.findAllById(listaIdUsuarios);

		Optional<Categoria> cat = categoriaRepository.findById(idCategoria);
		Categoria categoria = new Categoria();

		Optional<Notificacion> not = notificacionRepository.findById(idNotificacion);
		Notificacion notificacion = new Notificacion();

		if (!cat.isEmpty())
			categoria = cat.get();
		else {
			categoria.setDescripcion(nnudto.getDescripcionCategoria());
			categoria = categoriaRepository.save(categoria);
		}

		if (!not.isEmpty()) {
			notificacion = not.get();
		} else {
			notificacion.setCategoria(categoria);
			notificacion.setDescripcion(nnudto.getDescripcionNotificacion());
			notificacion.setEstado(true);
			notificacion = notificacionRepository.save(notificacion);
		}

		List<UsuarioNotificacion> listaUN = new ArrayList<UsuarioNotificacion>();
		for (Usuario usuario : listaUsuarios) {
			UsuarioNotificacion un = new UsuarioNotificacion();
			un.setNotificacion(notificacion);
			un.setUsuario(usuario);
			un.setFecha(LocalDate.now());

			listaUN.add(un);
		}

		usuarioNotificacionRepository.saveAll(listaUN);
	}

	@Override
	public void marcarNotificacionComoLeida(Long idUsuarioNotificacion) {
		
		UsuarioNotificacion un = usuarioNotificacionRepository.findById(idUsuarioNotificacion).get();
		
		un.setEstado(true);

		usuarioNotificacionRepository.save(un);
		
	}

	@Override
	public List<Categoria> obtenerCategorias() {
		// TODO Auto-generated method stub
		return (List<Categoria>) categoriaRepository.findAll();
	}

}
