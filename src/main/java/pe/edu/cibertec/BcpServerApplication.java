package pe.edu.cibertec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BcpServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BcpServerApplication.class, args);
	}

}
