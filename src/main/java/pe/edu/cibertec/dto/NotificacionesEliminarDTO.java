package pe.edu.cibertec.dto;

import java.util.List;

import lombok.Data;

@Data
public class NotificacionesEliminarDTO {

	private List<Long> listaNotUsuIds;
}
