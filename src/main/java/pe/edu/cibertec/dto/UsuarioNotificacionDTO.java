package pe.edu.cibertec.dto;

import java.util.List;

import lombok.Data;
import pe.edu.cibertec.model.UsuarioNotificacion;

@Data
public class UsuarioNotificacionDTO {

	private List<UsuarioNotificacion> listaNotificaciones;
	private Long cantidadNoLeidas;
}
