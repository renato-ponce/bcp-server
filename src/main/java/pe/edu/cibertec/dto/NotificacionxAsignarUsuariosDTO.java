package pe.edu.cibertec.dto;

import java.util.List;

import lombok.Data;

@Data
public class NotificacionxAsignarUsuariosDTO {

	private List<Long> listaUsuarios;
	private Long idCategoria;
	private String descripcionCategoria;
	private Long idNotificacion;
	private String descripcionNotificacion;
}
