package pe.edu.cibertec.dto;

import lombok.Data;

@Data
public class UsuarioValidarLoginDTO {

	private Long tarjeta;
	private int password;
}
