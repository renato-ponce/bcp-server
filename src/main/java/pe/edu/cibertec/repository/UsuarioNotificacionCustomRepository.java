package pe.edu.cibertec.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.edu.cibertec.model.Categoria;
import pe.edu.cibertec.model.Usuario;
import pe.edu.cibertec.model.UsuarioNotificacion;

@Repository
public interface UsuarioNotificacionCustomRepository extends JpaRepository<UsuarioNotificacion, Long> {

	public List<UsuarioNotificacion> findByUsuarioAndNotificacion_CategoriaOrderByFechaDesc(Usuario usuario,Categoria categoria); 

    public List<UsuarioNotificacion> findByUsuarioOrderByFechaDesc(Usuario usuario); 
    
    public void deleteByUsuNotIdIn(List<Long> listaIds);
}
