package pe.edu.cibertec.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.edu.cibertec.model.UsuarioNotificacion;

public interface UsuarioNotificacionRepository extends JpaRepository<UsuarioNotificacion, Long> {

}
