package pe.edu.cibertec.repository;

import org.springframework.data.repository.CrudRepository;

import pe.edu.cibertec.model.Notificacion;

public interface NotificacionRepository extends CrudRepository<Notificacion, Long> {

}
