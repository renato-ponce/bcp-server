package pe.edu.cibertec.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pe.edu.cibertec.model.Categoria;

@Repository
public interface CategoriaRepository extends CrudRepository<Categoria, Long> {

}
