package pe.edu.cibertec.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.edu.cibertec.model.Categoria;
import pe.edu.cibertec.model.Notificacion;

public interface NotificacionCustomRepository extends JpaRepository<Notificacion, Long> {

    public List<Notificacion> findByCategoria(Categoria categoria);
}
