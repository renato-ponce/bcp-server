package pe.edu.cibertec.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.edu.cibertec.model.Usuario;

@Repository
public interface UsuarioCustomRepository extends JpaRepository<Usuario, Long> {

	public List<Usuario> findByNombre(String nombre);

	public Usuario findByTarjetaAndPassword(Long tarjeta, int password);
}
