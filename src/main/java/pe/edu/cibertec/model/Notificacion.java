package pe.edu.cibertec.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "notificacion")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notificacion {

	@Id
	@Column(name="idNotificacion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idNotificacion;

	@Column(length = 100, nullable = false)
	private String descripcion;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column( length = 100, nullable = true)
	private LocalDate fecha;
	
	@Column(length = 100, nullable = false)
	private boolean estado;
	
	@ManyToOne
	@JoinColumn(name = "idCategoria",referencedColumnName = "idCategoria")
	private Categoria categoria;
}
