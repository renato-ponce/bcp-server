package pe.edu.cibertec.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "usuario_notificacion")
public class UsuarioNotificacion {

	@Id
	@Column(name="usuNotId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long usuNotId;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario",referencedColumnName = "idUsuario")
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "idNotificacion",referencedColumnName = "idNotificacion")
	private Notificacion notificacion;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column( length = 100, nullable = true)
	private LocalDate fecha;
	
	private boolean estado;
	
	
	@PrePersist
	void asignarValores() {
		fecha = LocalDate.now();
		estado = false;
	}

}
