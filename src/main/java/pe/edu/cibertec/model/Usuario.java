package pe.edu.cibertec.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "usuario")
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {

	@Id
	@Column(name="idUsuario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUsuario;

	@Column( nullable = false, length = 200)
	private String nombre;
	
	@Column(nullable = false, length = 200)
	private String apellido;
	
	@Column(nullable = false, length = 200)
	private String email;
	
	@Column(nullable = false, length = 6)
	private int password;
	
	
	@Column(nullable = false, length = 16)
	private Long tarjeta;	
	
	@PrePersist
	void asignarTarjeta() {
		tarjeta = AutoGenerarNumeroTarjeta();
	}
	
	public Long AutoGenerarNumeroTarjeta() {
        int parte1 =(int) (100000000 * Math.random() + 1);
        int parte2=(int) (100000000 * Math.random() + 1);
    
        String completo = parte1 +"" + parte2;
        
        return Long.parseLong(completo);

	}
	
}
