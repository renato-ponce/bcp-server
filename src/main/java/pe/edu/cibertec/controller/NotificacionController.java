package pe.edu.cibertec.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.status;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.cibertec.dto.NotificacionesEliminarDTO;
import pe.edu.cibertec.dto.NotificacionxAsignarUsuariosDTO;
import pe.edu.cibertec.dto.UsuarioNotificacionDTO;
import pe.edu.cibertec.model.Categoria;
import pe.edu.cibertec.model.Notificacion;
import pe.edu.cibertec.service.NotificacionService;

@RestController
@RequestMapping("/api/notificacion")
@CrossOrigin(origins = "http://localhost:4200")
public class NotificacionController {

	@Autowired
	private NotificacionService notificacionService;
	
	@GetMapping
	public ResponseEntity<List<Notificacion>> obtenerNotificacionesxCategoria(Long idCategoria) {
		
		List<Notificacion> lista = notificacionService.obtenerNotificacionesxCategoria(idCategoria);
		
		 return status(HttpStatus.OK).body(lista);
	}
	
	@GetMapping("/notificacionUsu")
	public ResponseEntity<UsuarioNotificacionDTO> obtenerNotificacionesxUsuarioxCategoria(Long idUsuario, Long idCategoria) {
		
		UsuarioNotificacionDTO u = notificacionService.obtenerNotificacionesxUsuarioxCategoria(idUsuario, idCategoria);
		 return status(HttpStatus.OK).body(u);
	}
	
	@PostMapping("/eliminarNot")
	public ResponseEntity<String> eliminarNotificacionesUsuario(@RequestBody NotificacionesEliminarDTO nedto) {
		
		notificacionService.eliminarNotificacionUsuario(nedto.getListaNotUsuIds());
		
		return new ResponseEntity<String>("Notificacion(es) Eliminada(s)",CREATED);
	}
	
	@PostMapping("/enviarNot")
	public ResponseEntity<String> enviarNotificacionUsuarios(@RequestBody NotificacionxAsignarUsuariosDTO nndto) {
		notificacionService.enviarNotificacionUsuarios(nndto);
		
		return new ResponseEntity<String>("La Notificación ha sido enviada.",CREATED);
	}
	
	@GetMapping("/marcarLeido")
	public ResponseEntity<String> marcarNotificacionComoLeida(Long idUsuarioNotificacion) {
		
		notificacionService.marcarNotificacionComoLeida(idUsuarioNotificacion);
		
		return new ResponseEntity<String>("Notificación marcada como Leida",CREATED);

	}
	@GetMapping("/categorias")
	public ResponseEntity<List<Categoria>> obtenerCategorias() {
		
		List<Categoria> lista = notificacionService.obtenerCategorias();
		
		 return status(HttpStatus.OK).body(lista);
	}
}
