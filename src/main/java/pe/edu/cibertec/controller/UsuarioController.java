package pe.edu.cibertec.controller;

import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.status;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.cibertec.dto.UsuarioValidarLoginDTO;
import pe.edu.cibertec.model.Usuario;
import pe.edu.cibertec.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
@CrossOrigin(origins = "http://localhost:4200")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/insertarUsu")
	public ResponseEntity<String> insertarUsuario(@RequestBody Usuario usuario){
		usuarioService.insertarUsuario(usuario);
        return new ResponseEntity<String>("¡Se ha registrado exitosamente!", CREATED);
	}
	
	@PostMapping("/actualizarUsu")
	public ResponseEntity<Usuario> actualizarUsuario(@RequestBody Usuario usuario){
		Usuario u = usuarioService.actualizarUsuario(usuario);
        return status(HttpStatus.OK).body(u);
	}
	
	@PostMapping
	public ResponseEntity<Usuario> validarLogin(@RequestBody UsuarioValidarLoginDTO uvdto){

		Usuario u = usuarioService.validarLogin(uvdto.getTarjeta(), uvdto.getPassword()); 
		 return status(HttpStatus.OK).body(u);
	}
}
