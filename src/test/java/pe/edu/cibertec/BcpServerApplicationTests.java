package pe.edu.cibertec;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.edu.cibertec.dto.NotificacionxAsignarUsuariosDTO;
import pe.edu.cibertec.model.Categoria;
import pe.edu.cibertec.model.Notificacion;
import pe.edu.cibertec.model.Usuario;
import pe.edu.cibertec.model.UsuarioNotificacion;
import pe.edu.cibertec.repository.CategoriaRepository;
import pe.edu.cibertec.repository.NotificacionCustomRepository;
import pe.edu.cibertec.repository.NotificacionRepository;
import pe.edu.cibertec.repository.UsuarioCustomRepository;
import pe.edu.cibertec.repository.UsuarioNotificacionCustomRepository;
import pe.edu.cibertec.repository.UsuarioNotificacionRepository;
import pe.edu.cibertec.repository.UsuarioRepository;
import pe.edu.cibertec.service.NotificacionService;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class BcpServerApplicationTests {

	@Autowired
	private NotificacionRepository notificacionRepository;
	@Autowired
	private CategoriaRepository categoriaRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Test
	public void insertarUsuario() {
		Usuario u = new Usuario(null, "Jose", "Miranda Lopez", "jose@hotmail.com", 654321, null);
		Usuario u2 =  usuarioRepository.save(u);
		assertNotNull(u2);
	}
	
	@Test
	public void primeraEjecucion() {
		Categoria categoria = new Categoria();
		categoria.setDescripcion("Seguridad");

		Categoria categoriainsertada = categoriaRepository.save(categoria);

		Notificacion notificacion = new Notificacion();
		notificacion.setCategoria(categoriainsertada);
		notificacion.setDescripcion("Usted ha actualizado sus datos");
		notificacion.setFecha(LocalDate.now());
		notificacion.setEstado(true);

		Notificacion notificacioninsertada = notificacionRepository.save(notificacion);

		assertNotNull(categoriainsertada);

		assertNotNull(notificacioninsertada);
	}
	

}
